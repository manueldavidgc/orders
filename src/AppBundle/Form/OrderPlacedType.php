<?php

namespace AppBundle\Form;

use AppBundle\Entity\Client;
use AppBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderPlacedType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('products',EntityType::class, [
                'label' => 'label.products',
                'placeholder' => 'placeholder.select_product',
                'class' => Product::class,
                'multiple' => true,
                'choice_attr' => function($val, $key, $index) {
                    return ['data-price' => $val->getPrice()];
                },
            ])
            ->add('client', EntityType::class, [
                'label' => 'label.client',
                'placeholder' => 'placeholder.select_client',
                'class' => Client::class
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\OrderPlaced'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_orderplaced';
    }


}
