<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * OrderPlaced
 *
 * @ORM\Table(name="order_placed")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderPlacedRepository")
 */
class OrderPlaced
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"order_list"})
     */
    private $id;

    /**
     * @var \DateTime
     * @Groups({"order_list"})
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var float
     * @Groups({"order_list"})
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var ArrayCollection
     * @Groups({"order_list"})
     * @ORM\ManyToMany(targetEntity="Product")
     * @ORM\JoinTable(name="orders_products",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    private $products;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="ordersPlaced")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    public function __construct() 
    {
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return OrderPlaced
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return OrderPlaced
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param ArrayCollection $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return ArrayCollection
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param ArrayCollection $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }
}

