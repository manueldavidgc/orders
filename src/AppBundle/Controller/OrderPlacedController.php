<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OrderPlaced;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Orderplaced controller.
 *
 * @Route("order-placed")
 */
class OrderPlacedController extends Controller
{
    /**
     * Lists all orderPlaced entities.
     *
     * @Route("/", name="orderplaced_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $orderPlaceds = $em->getRepository('AppBundle:OrderPlaced')->findAll();

        return $this->render('orderplaced/index.html.twig', array(
            'orderPlaceds' => $orderPlaceds,
        ));
    }

    /**
     * Creates a new orderPlaced entity.
     *
     * @Route("/new", name="orderplaced_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $orderPlaced = new Orderplaced();
        $form = $this->createForm('AppBundle\Form\OrderPlacedType', $orderPlaced);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->updateOrderPlaced($orderPlaced);

            $em = $this->getDoctrine()->getManager();
            $em->persist($orderPlaced);
            $em->flush();

            return $this->redirectToRoute('orderplaced_index', array('id' => $orderPlaced->getId()));
        }

        return $this->render('orderplaced/new.html.twig', array(
            'orderPlaced' => $orderPlaced,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing orderPlaced entity.
     *
     * @Route("/{id}/edit", name="orderplaced_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OrderPlaced $orderPlaced)
    {
        $deleteForm = $this->createDeleteForm($orderPlaced);
        $editForm = $this->createForm('AppBundle\Form\OrderPlacedType', $orderPlaced);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->updateOrderPlaced($orderPlaced);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('orderplaced_edit', array('id' => $orderPlaced->getId()));
        }

        return $this->render('orderplaced/edit.html.twig', array(
            'orderPlaced' => $orderPlaced,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a orderPlaced entity.
     *
     * @Route("/{id}", name="orderplaced_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OrderPlaced $orderPlaced)
    {
        $form = $this->createDeleteForm($orderPlaced);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($orderPlaced);
            $em->flush();
        }

        return $this->redirectToRoute('orderplaced_index');
    }

    /**
     * Creates a form to delete a orderPlaced entity.
     *
     * @param OrderPlaced $orderPlaced The orderPlaced entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OrderPlaced $orderPlaced)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('orderplaced_delete', array('id' => $orderPlaced->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @param OrderPlaced $orderPlaced
     * @throws \Exception
     */
    public function updateOrderPlaced(OrderPlaced $orderPlaced)
    {
        $finalValue = 0;
        /** @var Product $product */
        foreach ($orderPlaced->getProducts() as $product) {
            $finalValue += $product->getPrice();
        }

        $orderPlaced->setAmount($finalValue);
        $orderPlaced->setCreated(new \DateTime('now'));
    }
}
