<?php


namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * Lists default controller
     *
     * @Route("/", name="home")
     */
    public function homeAction()
    {
        return $this->render('default/index.html.twig');
    }
}