<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\OrderPlaced;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/orders/list/{clientId}", methods={"GET"})
     * @param Request $request
     * @param int $clientId
     * @return JsonResponse|Response
     */
    public function listAction(Request $request, int $clientId)
    {
        $client = $this->getDoctrine()->getRepository(Client::class)->findOneBy(
            ['id' => $clientId]
        );

        if (empty($client)) {
            return new JsonResponse(['Not Found'], Response::HTTP_NOT_FOUND);
        }

        $orders = $this->getDoctrine()->getRepository(OrderPlaced::class)->findBy(
            ['client' => $client]
        );

        $serializer = $this->get('serializer');

        return new Response($serializer->serialize($orders,'json', ['groups' => ['order_list']]));
    }

}
