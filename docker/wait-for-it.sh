#!/bin/sh

set -e

host="$1"
shift
cmd="$@"

until nc -zvw3 $host 3306; do
  >&2 echo "mysql is unavailable - sleeping"
  sleep 1
done

>&2 echo "mysql is up - executing command"
exec $cmd