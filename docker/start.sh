#!/usr/bin/env sh

cd /usr/src/app

echo "Start doctrine schema generate"
php bin/console c:c --env=prod
php bin/console doc:sch:upd --dump-sql --force

chmod 777 -R var/cache var/logs var/sessions

exec php-fpm